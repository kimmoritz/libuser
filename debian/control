Source: libuser
Section: admin
Priority: optional
Maintainer: Ghe Rivero <ghe@debian.org>
Uploaders:
 Tzafrir Cohen <tzafrir@debian.org>,
Build-Depends:
 cpio,
 debhelper-compat (= 13),
 gtk-doc-tools,
 groff,
 libglib2.0-dev,
 libpam0g-dev,
 libpopt-dev,
 linuxdoc-tools,
 pkg-config,
Standards-Version: 4.5.1
Homepage: https://pagure.io/libuser/
Vcs-Git: https://salsa.debian.org/debian/libuser.git
Vcs-Browser: https://salsa.debian.org/debian/libuser

Package: libuser
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: user and group account administration library - utilities
 The libuser library implements a standardized interface for manipulating
 and administering user and group accounts.  The library uses pluggable
 back-ends to interface to its data sources.
 .
 Sample applications modeled after those included with the shadow password
 suite are included: lchsh, lchfn, lid, lnewusers, lgroupadd, luseradd,
 lgroupdel, luserdel, lusermod, lgroupmod, lchage and lpasswd.

Package: libuser1-dev
Architecture: any
Depends:
 libuser1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Section: libdevel
Description: user and group account administration library - development files
 The libuser library implements a standardized interface for manipulating
 and administering user and group accounts.  The library uses pluggable
 back-ends to interface to its data sources.
 .
 This package contains the development headers and data.

Package: libuser1
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Section: libs
Description: user and group account administration library - shared libraries
 The libuser library implements a standardized interface for manipulating
 and administering user and group accounts.  The library uses pluggable
 back-ends to interface to its data sources.
 .
 This package contains the shared library itself.
