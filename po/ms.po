# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Red Hat, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Miloslav Trmač <mitr@volny.cz>, 2011
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2004, 2006
msgid ""
msgstr ""
"Project-Id-Version: libuser 0.60\n"
"Report-Msgid-Bugs-To: http://bugzilla.redhat.com/bugzilla/\n"
"POT-Creation-Date: 2015-07-23 21:13+0200\n"
"PO-Revision-Date: 2013-04-29 04:37-0400\n"
"Last-Translator: Miloslav Trmač <mitr@volny.cz>\n"
"Language-Team: Malay (http://www.transifex.com/projects/p/fedora/language/"
"ms/)\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Zanata 3.6.2\n"

#: apps/apputil.c:197 apps/apputil.c:201
#, c-format
msgid "Failed to drop privileges.\n"
msgstr "Gagal untuk mengurangkan kepentingan.\n"

#: apps/apputil.c:210
#, c-format
msgid "Internal error.\n"
msgstr "Ralat dalaman.\n"

#: apps/apputil.c:231
#, c-format
msgid "%s is not authorized to change the finger info of %s\n"
msgstr "%s tidak dibenarkan untuk mengubah maklumat finger bagi %s\n"

#: apps/apputil.c:233
msgid "Unknown user context"
msgstr "Konteks pengguna tidak diketahui"

#: apps/apputil.c:241
#, c-format
msgid "Can't set default context for /etc/passwd\n"
msgstr "Tidak dapat menetapkan konteks default untuk /etc/passwd\n"

#: apps/apputil.c:250
#, c-format
msgid "Error initializing PAM.\n"
msgstr "Ralat memulakan PAM.\n"

#: apps/apputil.c:260 apps/apputil.c:287
#, c-format
msgid "Authentication failed for %s.\n"
msgstr "Pengesahan gagal untuk %s.\n"

#: apps/apputil.c:268
#, c-format
msgid "Internal PAM error `%s'.\n"
msgstr "Ralat dalaman PAM `%s'.\n"

#: apps/apputil.c:273
#, c-format
msgid "Unknown user authenticated.\n"
msgstr "Pengguna tidak diketahui disahkan.\n"

#: apps/apputil.c:277
#, c-format
msgid "User mismatch.\n"
msgstr "Pengguna tidak sepadan.\n"

#: apps/lchage.c:84 apps/lchfn.c:53 apps/lchsh.c:44 apps/lgroupadd.c:47
#: apps/lgroupdel.c:42 apps/lgroupmod.c:54 apps/lid.c:115 apps/lnewusers.c:45
#: apps/lpasswd.c:47 apps/luseradd.c:55 apps/luserdel.c:45 apps/lusermod.c:56
msgid "prompt for all information"
msgstr ""

#: apps/lchage.c:86
msgid "list aging parameters for the user"
msgstr ""

#: apps/lchage.c:88
msgid "minimum days between password changes"
msgstr ""

#: apps/lchage.c:88 apps/lchage.c:90 apps/lchage.c:93 apps/lchage.c:96
#: apps/lchage.c:99 apps/lchage.c:102
msgid "DAYS"
msgstr ""

#: apps/lchage.c:90
msgid "maximum days between password changes"
msgstr ""

#: apps/lchage.c:92
msgid "date of last password change in days since 1/1/70"
msgstr ""

#: apps/lchage.c:95
msgid ""
"number of days after password expiration date when account is considered "
"inactive"
msgstr ""

#: apps/lchage.c:98
msgid "password expiration date in days since 1/1/70"
msgstr ""

#: apps/lchage.c:101
msgid "days before expiration to begin warning user"
msgstr ""

#: apps/lchage.c:114 apps/lid.c:130 apps/lpasswd.c:68 apps/luseradd.c:100
#: apps/luserdel.c:59 apps/lusermod.c:101
msgid "[OPTION...] user"
msgstr "[PILIHAN...] pengguna"

#: apps/lchage.c:117 apps/lchfn.c:70 apps/lchsh.c:58 apps/lgroupadd.c:65
#: apps/lgroupdel.c:56 apps/lgroupmod.c:85 apps/lid.c:133 apps/lnewusers.c:66
#: apps/lpasswd.c:71 apps/luseradd.c:103 apps/luserdel.c:62
#: apps/lusermod.c:104
#, c-format
msgid "Error parsing arguments: %s.\n"
msgstr "Ralat hujah penelitian: %s.\n"

#: apps/lchage.c:126 apps/lpasswd.c:85 apps/luseradd.c:118 apps/luserdel.c:70
#: apps/lusermod.c:115
#, c-format
msgid "No user name specified.\n"
msgstr "Tiada nama pengguna dinyatakan.\n"

#: apps/lchage.c:138 apps/lchfn.c:103 apps/lchsh.c:91 apps/lgroupadd.c:102
#: apps/lgroupdel.c:77 apps/lgroupmod.c:119 apps/lid.c:179 apps/lnewusers.c:79
#: apps/lpasswd.c:97 apps/luseradd.c:143 apps/luserdel.c:81
#: apps/lusermod.c:157 samples/enum.c:56 samples/testuser.c:71
#, c-format
msgid "Error initializing %s: %s.\n"
msgstr "Ralat memulakan %s: %s.\n"

#: apps/lchage.c:147 apps/lchfn.c:114 apps/lchsh.c:102 apps/lpasswd.c:148
#: apps/luserdel.c:89 apps/lusermod.c:171
#, c-format
msgid "User %s does not exist.\n"
msgstr "Pengguna %s tidak wujud.\n"

#: apps/lchage.c:160
#, c-format
msgid "Account is locked.\n"
msgstr "Akaun dikunci.\n"

#: apps/lchage.c:162
#, c-format
msgid "Account is not locked.\n"
msgstr "Akaun tidak dikunci.\n"

#: apps/lchage.c:166
#, c-format
msgid "Minimum:\t%ld\n"
msgstr "Minimum:\t%ld\n"

#: apps/lchage.c:170
#, c-format
msgid "Maximum:\t%ld\n"
msgstr "Maksimum:\t%ld\n"

#: apps/lchage.c:172
#, c-format
msgid "Maximum:\tNone\n"
msgstr ""

#: apps/lchage.c:175
#, c-format
msgid "Warning:\t%ld\n"
msgstr "Amaran:\t%ld\n"

#: apps/lchage.c:180
#, c-format
msgid "Inactive:\t%ld\n"
msgstr "Tidak aktif:\t%ld\n"

#: apps/lchage.c:182
#, c-format
msgid "Inactive:\tNever\n"
msgstr ""

#: apps/lchage.c:186 apps/lchage.c:194 apps/lchage.c:204
msgid "Must change password on next login"
msgstr ""

#: apps/lchage.c:188 apps/lchage.c:196 apps/lchage.c:206 apps/lchage.c:215
msgid "Never"
msgstr "Tidak sekali"

#: apps/lchage.c:191
#, c-format
msgid "Last Change:\t%s\n"
msgstr "Perubahan Terakhir:\t%s\n"

#: apps/lchage.c:201
#, c-format
msgid "Password Expires:\t%s\n"
msgstr "Katalaluan Luput:\t%s\n"

#: apps/lchage.c:213
#, c-format
msgid "Password Inactive:\t%s\n"
msgstr "Katalaluan Tidak Aktif:\t%s\n"

#: apps/lchage.c:219
#, c-format
msgid "Account Expires:\t%s\n"
msgstr "Akaun Luput:\t%s\n"

#: apps/lchage.c:240
#, c-format
msgid "Failed to modify aging information for %s: %s\n"
msgstr "Gagal menukar maklumat usia untuk %s: %s\n"

#: apps/lchfn.c:67 apps/lchsh.c:55
msgid "[OPTION...] [user]"
msgstr "[PILIHAN...] [pengguna]"

#: apps/lchfn.c:86 apps/lchsh.c:74 apps/lid.c:164
#, c-format
msgid "No user name specified, no name for uid %d.\n"
msgstr "Tiada pengguna dinyatakan, tiada nama untuk uid %d.\n"

#: apps/lchfn.c:96
#, c-format
msgid "Changing finger information for %s.\n"
msgstr "Menukar maklumat finger untuk %s.\n"

#: apps/lchfn.c:136
msgid "Full Name"
msgstr "Nama Penuh"

#: apps/lchfn.c:146
msgid "Surname"
msgstr "Nama keluarga"

#: apps/lchfn.c:157
msgid "Given Name"
msgstr "Nama Diberi"

#: apps/lchfn.c:167
msgid "Office"
msgstr "Pejabat"

#: apps/lchfn.c:176
msgid "Office Phone"
msgstr "Telefon Pejabat"

#: apps/lchfn.c:185
msgid "Home Phone"
msgstr "Telefon Rumah"

#: apps/lchfn.c:195
msgid "E-Mail Address"
msgstr "Alamat E-Mel"

#: apps/lchfn.c:208
#, c-format
msgid "Finger information not changed:  input error.\n"
msgstr "Maklumat finger tidak bertukar:  ralat masukan.\n"

#: apps/lchfn.c:270
msgid "Finger information changed.\n"
msgstr "Maklumat finger berubah.\n"

#: apps/lchfn.c:273
#, c-format
msgid "Finger information not changed: %s.\n"
msgstr "Maklumat finger tidak berubah: %s.\n"

#: apps/lchsh.c:84
#, c-format
msgid "Changing shell for %s.\n"
msgstr "Menukar shell untuk %s.\n"

#: apps/lchsh.c:114
msgid "New Shell"
msgstr "Shell Baru"

#: apps/lchsh.c:121 apps/lchsh.c:136
#, c-format
msgid "Shell not changed: %s\n"
msgstr "Shell tidak berubah: %s\n"

#: apps/lchsh.c:133
msgid "Shell changed.\n"
msgstr "Shell berubah.\n"

#: apps/lgroupadd.c:49
msgid "gid for new group"
msgstr ""

#: apps/lgroupadd.c:49 apps/lgroupmod.c:56 apps/lpasswd.c:56 apps/lpasswd.c:59
#: apps/luseradd.c:67 apps/lusermod.c:66 apps/lusermod.c:68
msgid "NUM"
msgstr ""

#: apps/lgroupadd.c:51
msgid "create a system group"
msgstr ""

#: apps/lgroupadd.c:62 apps/lgroupdel.c:53 apps/lgroupmod.c:82
msgid "[OPTION...] group"
msgstr "[OPTION...] kumpulan"

#: apps/lgroupadd.c:74 apps/lgroupdel.c:65 apps/lgroupmod.c:93
#, c-format
msgid "No group name specified.\n"
msgstr "Tiada nama kumpulan dinyatakan.\n"

#: apps/lgroupadd.c:87 apps/lgroupmod.c:105 apps/lnewusers.c:172
#: apps/luseradd.c:164 apps/lusermod.c:127
#, c-format
msgid "Invalid group ID %s\n"
msgstr "ID kumpulan %s tidak sah\n"

#: apps/lgroupadd.c:119
#, c-format
msgid "Group creation failed: %s\n"
msgstr "Gagal mencipta kumpulan: %s\n"

#: apps/lgroupdel.c:85 apps/lgroupmod.c:132 apps/lpasswd.c:153
#, c-format
msgid "Group %s does not exist.\n"
msgstr "Kumpulan %s tidak wujud.\n"

#: apps/lgroupdel.c:91
#, c-format
msgid "Group %s could not be deleted: %s\n"
msgstr "Kumpulan %s tidak dapat dipadam: %s\n"

#: apps/lgroupmod.c:56
msgid "set GID for group"
msgstr ""

#: apps/lgroupmod.c:58
msgid "change group to have given name"
msgstr ""

#: apps/lgroupmod.c:58
msgid "NAME"
msgstr ""

#: apps/lgroupmod.c:60 apps/luseradd.c:75
msgid "plaintext password for use with group"
msgstr ""

#: apps/lgroupmod.c:60 apps/lgroupmod.c:62 apps/lgroupmod.c:64
#: apps/lgroupmod.c:66 apps/lgroupmod.c:68 apps/lgroupmod.c:70
#: apps/lpasswd.c:51 apps/lpasswd.c:53 apps/luseradd.c:59 apps/luseradd.c:61
#: apps/luseradd.c:63 apps/luseradd.c:65 apps/luseradd.c:69 apps/luseradd.c:75
#: apps/luseradd.c:77 apps/luseradd.c:79 apps/luseradd.c:81 apps/luseradd.c:83
#: apps/luseradd.c:85 apps/luseradd.c:87 apps/luseradd.c:89 apps/lusermod.c:58
#: apps/lusermod.c:60 apps/lusermod.c:64 apps/lusermod.c:70 apps/lusermod.c:72
#: apps/lusermod.c:74 apps/lusermod.c:80 apps/lusermod.c:82 apps/lusermod.c:84
#: apps/lusermod.c:86 apps/lusermod.c:88 apps/lusermod.c:90
msgid "STRING"
msgstr ""

#: apps/lgroupmod.c:62 apps/luseradd.c:77
msgid "pre-hashed password for use with group"
msgstr ""

#: apps/lgroupmod.c:64
msgid "list of administrators to add"
msgstr ""

#: apps/lgroupmod.c:66
msgid "list of administrators to remove"
msgstr ""

#: apps/lgroupmod.c:68
msgid "list of group members to add"
msgstr ""

#: apps/lgroupmod.c:70
msgid "list of group members to remove"
msgstr ""

#: apps/lgroupmod.c:71
msgid "lock group"
msgstr ""

#: apps/lgroupmod.c:72
msgid "unlock group"
msgstr ""

#: apps/lgroupmod.c:125 apps/lusermod.c:164
#, c-format
msgid "Both -L and -U specified.\n"
msgstr "Kedua-dua L dan U dinyatakan.\n"

#: apps/lgroupmod.c:139 apps/lgroupmod.c:148
#, c-format
msgid "Failed to set password for group %s: %s\n"
msgstr "Gagal untuk menetapkan katalaluan bagi kumpulan %s: %s\n"

#: apps/lgroupmod.c:157
#, c-format
msgid "Group %s could not be locked: %s\n"
msgstr "Kumpulan %s tidak dapat dikunci: %s\n"

#: apps/lgroupmod.c:166
#, c-format
msgid "Group %s could not be unlocked: %s\n"
msgstr "Kumpulan %s tidak dapat dinyahkunci: %s\n"

#: apps/lgroupmod.c:242 apps/lgroupmod.c:257
#, c-format
msgid "Group %s could not be modified: %s\n"
msgstr "Kumpulan %s tidak dapat diubahsuai: %s\n"

#: apps/lid.c:42 apps/lid.c:74 apps/lid.c:188
#, c-format
msgid "Error looking up %s: %s\n"
msgstr "Ralat melihat %s: %s\n"

#: apps/lid.c:117
msgid ""
"list members of a named group instead of the group memberships for the named "
"user"
msgstr ""

#: apps/lid.c:120
msgid "only list membership information by name, and not UID/GID"
msgstr ""

#: apps/lid.c:146
#, c-format
msgid "No group name specified, using %s.\n"
msgstr "Tiada nama kumpulan dinyatakan, menggunakan %s.\n"

#: apps/lid.c:150
#, c-format
msgid "No group name specified, no name for gid %d.\n"
msgstr "Tiada nama kumpulan dinyatakan, tiada nama untuk gid %d.\n"

#: apps/lid.c:160
#, c-format
msgid "No user name specified, using %s.\n"
msgstr "Tiada nama pengguna dinyatakan, menggunakan %s.\n"

#: apps/lid.c:192
#, c-format
msgid "%s does not exist\n"
msgstr ""

#: apps/lnewusers.c:47
msgid "file with user information records"
msgstr ""

#: apps/lnewusers.c:47
msgid "PATH"
msgstr ""

#: apps/lnewusers.c:49
msgid "don't create home directories"
msgstr ""

#: apps/lnewusers.c:51
msgid "don't create mail spools"
msgstr ""

#: apps/lnewusers.c:63
msgid "[OPTION...]"
msgstr "[PILIHAN...]"

#: apps/lnewusers.c:88
#, c-format
msgid "Error opening `%s': %s.\n"
msgstr "Ralat membuka `%s': %s.\n"

#: apps/lnewusers.c:118
#, c-format
msgid "Error creating account for `%s': line improperly formatted.\n"
msgstr "Ralat mencipta akaun untuk `%s': baris tidak diformat dengan betul.\n"

#: apps/lnewusers.c:129 apps/luseradd.c:128 apps/lusermod.c:142
#, c-format
msgid "Invalid user ID %s\n"
msgstr "ID pengguna tidak sah %s\n"

#: apps/lnewusers.c:136
msgid "Refusing to create account with UID 0.\n"
msgstr "Enggan mencipta akaun dengan UID 0.\n"

#: apps/lnewusers.c:206
#, c-format
msgid "Error creating group for `%s' with GID %jd: %s\n"
msgstr "Ralat mencipta kumpulan untuk `%s' dengan GID %jd: %s\n"

#: apps/lnewusers.c:246
#, c-format
msgid "Refusing to use dangerous home directory `%s' for %s by default\n"
msgstr ""

#: apps/lnewusers.c:257
#, c-format
msgid "Error creating home directory for %s: %s\n"
msgstr "Ralat mencipta direktori rumah untuk %s: %s\n"

#: apps/lnewusers.c:270
#, c-format
msgid "Error creating mail spool for %s: %s\n"
msgstr ""

#: apps/lnewusers.c:285
#, c-format
msgid "Error setting initial password for %s: %s\n"
msgstr "Ralat menetapkan katalaluan permulaan untuk %s: %s\n"

#: apps/lnewusers.c:295
#, c-format
msgid "Error creating user account for %s: %s\n"
msgstr "Ralat mencipta akaun pengguna untuk %s: %s\n"

#: apps/lpasswd.c:49
msgid "set group password instead of user password"
msgstr ""

#: apps/lpasswd.c:51
msgid "new plain password"
msgstr ""

#: apps/lpasswd.c:53
msgid "new crypted password"
msgstr ""

#: apps/lpasswd.c:55
msgid "read new plain password from given descriptor"
msgstr ""

#: apps/lpasswd.c:58
msgid "read new crypted password from given descriptor"
msgstr ""

#: apps/lpasswd.c:83
#, c-format
msgid "Changing password for %s.\n"
msgstr "Menukar katalaluan untuk %s.\n"

#: apps/lpasswd.c:111
msgid "New password"
msgstr "Katalaluan baru"

#: apps/lpasswd.c:114
msgid "New password (confirm)"
msgstr "Katalaluan baru (sahkan)"

#: apps/lpasswd.c:128
#, c-format
msgid "Passwords do not match, try again.\n"
msgstr "Katalaluan tidak sepadan, cuba lagi.\n"

#: apps/lpasswd.c:133
#, c-format
msgid "Password change canceled.\n"
msgstr "Pertukaran katalaluan dibatalkan.\n"

#: apps/lpasswd.c:165 apps/lpasswd.c:182
#, c-format
msgid "Error reading from file descriptor %d.\n"
msgstr ""

#: apps/lpasswd.c:203 apps/luseradd.c:302 apps/luseradd.c:311
#, c-format
msgid "Error setting password for user %s: %s.\n"
msgstr "Ralat menetapkan katalaluan untuk pengguna %s: %s.\n"

#: apps/lpasswd.c:212
#, c-format
msgid "Error setting password for group %s: %s.\n"
msgstr "Ralat menetapkan katalaluan untuk kumpulan %s: %s.\n"

#: apps/lpasswd.c:224
#, c-format
msgid "Password changed.\n"
msgstr "Katalaluan ditukar.\n"

#: apps/luseradd.c:57
msgid "create a system user"
msgstr ""

#: apps/luseradd.c:59
msgid "GECOS information for new user"
msgstr ""

#: apps/luseradd.c:61
msgid "home directory for new user"
msgstr ""

#: apps/luseradd.c:63
msgid "directory with files for the new user"
msgstr ""

#: apps/luseradd.c:65
msgid "shell for new user"
msgstr ""

#: apps/luseradd.c:67
msgid "uid for new user"
msgstr ""

#: apps/luseradd.c:69
msgid "group for new user"
msgstr ""

#: apps/luseradd.c:71
msgid "don't create home directory for user"
msgstr ""

#: apps/luseradd.c:73
msgid "don't create group with same name as user"
msgstr ""

#: apps/luseradd.c:79
msgid "common name for new user"
msgstr ""

#: apps/luseradd.c:81
msgid "given name for new user"
msgstr ""

#: apps/luseradd.c:83
msgid "surname for new user"
msgstr ""

#: apps/luseradd.c:85
msgid "room number for new user"
msgstr ""

#: apps/luseradd.c:87
msgid "telephone number for new user"
msgstr ""

#: apps/luseradd.c:89
msgid "home telephone number for new user"
msgstr ""

#: apps/luseradd.c:189
#, c-format
msgid "Group %jd does not exist\n"
msgstr "Kumpulan %jd tidak wujud\n"

#: apps/luseradd.c:207 apps/luseradd.c:220
#, c-format
msgid "Error creating group `%s': %s\n"
msgstr "Ralat mencipta kumpulan `%s': %s\n"

#: apps/luseradd.c:260
#, c-format
msgid "Account creation failed: %s.\n"
msgstr "Gagal mencipta akaun: %s.\n"

#: apps/luseradd.c:283
#, c-format
msgid "Error creating %s: %s.\n"
msgstr "Ralat mencipta %s: %s.\n"

#: apps/luseradd.c:290
#, c-format
msgid "Error creating mail spool: %s\n"
msgstr ""

#: apps/luserdel.c:47
msgid "don't remove the user's private group, if the user has one"
msgstr ""

#: apps/luserdel.c:50
msgid "remove the user's home directory"
msgstr ""

#: apps/luserdel.c:94
#, c-format
msgid "User %s could not be deleted: %s.\n"
msgstr "Pengguna %s tidak dapat dipadam: %s.\n"

#: apps/luserdel.c:108
#, c-format
msgid "%s did not have a gid number.\n"
msgstr "%s tidak mempunyai nombor gid.\n"

#: apps/luserdel.c:114
#, c-format
msgid "No group with GID %jd exists, not removing.\n"
msgstr "Tiada kumpulan dengan GID %jd wujud, tidak membuang.\n"

#: apps/luserdel.c:120
#, c-format
msgid "Group with GID %jd did not have a group name.\n"
msgstr "Kumpulan dengan GID %jd tidak mempunyai nama kumpulan.\n"

#: apps/luserdel.c:126
#, c-format
msgid "Group %s could not be deleted: %s.\n"
msgstr "Kumpulan %s tidak dapat dipadam: %s.\n"

#: apps/luserdel.c:139
#, fuzzy, c-format
msgid "Error removing home directory: %s.\n"
msgstr "ralat membuang direktori rumah untuk pengguna"

#: apps/luserdel.c:145
#, c-format
msgid "Error removing mail spool: %s"
msgstr ""

#: apps/lusermod.c:58
msgid "GECOS information"
msgstr ""

#: apps/lusermod.c:60
msgid "home directory"
msgstr ""

#: apps/lusermod.c:62
msgid "move home directory contents"
msgstr ""

#: apps/lusermod.c:64
msgid "set shell for user"
msgstr ""

#: apps/lusermod.c:66
msgid "set UID for user"
msgstr ""

#: apps/lusermod.c:68
msgid "set primary GID for user"
msgstr ""

#: apps/lusermod.c:70
msgid "change login name for user"
msgstr ""

#: apps/lusermod.c:72
msgid "plaintext password for the user"
msgstr ""

#: apps/lusermod.c:74
msgid "pre-hashed password for the user"
msgstr ""

#: apps/lusermod.c:75
msgid "lock account"
msgstr ""

#: apps/lusermod.c:78
msgid "unlock account"
msgstr ""

#: apps/lusermod.c:80
msgid "set common name for user"
msgstr ""

#: apps/lusermod.c:82
msgid "set given name for user"
msgstr ""

#: apps/lusermod.c:84
msgid "set surname for user"
msgstr ""

#: apps/lusermod.c:86
msgid "set room number for user"
msgstr ""

#: apps/lusermod.c:88
msgid "set telephone number for user"
msgstr ""

#: apps/lusermod.c:90
msgid "set home telephone number for user"
msgstr ""

#: apps/lusermod.c:180 apps/lusermod.c:193
#, c-format
msgid "Failed to set password for user %s: %s.\n"
msgstr "Gagal untuk menetapkan katalaluan bagi pengguna %s: %s\n"

#: apps/lusermod.c:203
#, c-format
msgid "User %s could not be locked: %s.\n"
msgstr "Pengguna %s tidak dapat dikunci: %s.\n"

#: apps/lusermod.c:211
#, c-format
msgid "User %s could not be unlocked: %s.\n"
msgstr "Pengguna %s tidak dapat dinyahkunci: %s.\n"

#: apps/lusermod.c:232
#, c-format
msgid "Warning: Group with ID %jd does not exist.\n"
msgstr ""

#: apps/lusermod.c:275
#, c-format
msgid "User %s could not be modified: %s.\n"
msgstr "Pengguna %s tidak dapat diubahsuai: %s.\n"

#: apps/lusermod.c:326
#, c-format
msgid "Group %s could not be modified: %s.\n"
msgstr "Kumpulan %s tidak dapat diubahsuai: %s\n"

#: apps/lusermod.c:342
#, c-format
msgid "No old home directory for %s.\n"
msgstr "Tiada direktori rumah lama untuk %s.\n"

#: apps/lusermod.c:347
#, c-format
msgid "No new home directory for %s.\n"
msgstr "Tiada direktori rumah baru untuk %s.\n"

#: apps/lusermod.c:353
#, c-format
msgid "Error moving %s to %s: %s.\n"
msgstr "Ralat memindahkan %s ke %s: %s.\n"

#: lib/config.c:128
#, c-format
msgid "could not open configuration file `%s': %s"
msgstr "tidak dapat membuka fail tetapan `%s': %s"

#: lib/config.c:134
#, c-format
msgid "could not stat configuration file `%s': %s"
msgstr "tidak dapat stat fail tetapan `%s': %s"

#: lib/config.c:143
#, c-format
msgid "configuration file `%s' is too large"
msgstr "fail tetapan `%s' adalah terlalu besar"

#: lib/config.c:159
#, c-format
msgid "could not read configuration file `%s': %s"
msgstr "tidak dapat membaca fail tetapan `%s': %s"

#: lib/error.c:62
msgid "success"
msgstr "berjaya"

#: lib/error.c:64
msgid "module disabled by configuration"
msgstr "modul dimatikan oleh tetapan"

#: lib/error.c:66
msgid "generic error"
msgstr "ralat umum"

#: lib/error.c:68
msgid "not enough privileges"
msgstr "tidak cukup kelebihan"

#: lib/error.c:70
msgid "access denied"
msgstr "tiada kebenaran"

#: lib/error.c:72
msgid "bad user/group name"
msgstr "nama pengguna/kumpulan buruk"

#: lib/error.c:74
msgid "bad user/group id"
msgstr "id pengguna/kumpulan buruk"

#: lib/error.c:76
msgid "user/group name in use"
msgstr "nama pengguna/kumpulan sedang digunakan"

#: lib/error.c:78
msgid "user/group id in use"
msgstr "id pengguna/kumpulan sedang digunakan"

#: lib/error.c:80
msgid "error manipulating terminal attributes"
msgstr "ralat memanipulasi sifat terminal"

#: lib/error.c:82
msgid "error opening file"
msgstr "ralat membuka fail"

#: lib/error.c:84
msgid "error locking file"
msgstr "ralat mengunci fail"

#: lib/error.c:86
msgid "error statting file"
msgstr "ralat stat fail"

#: lib/error.c:88
msgid "error reading file"
msgstr "ralat membaca fail"

#: lib/error.c:90
msgid "error writing to file"
msgstr "ralat menulis ke fail"

#: lib/error.c:92
msgid "data not found in file"
msgstr "data tidak ditemui dalam fail"

#: lib/error.c:94
msgid "internal initialization error"
msgstr "ralat permulaan dalaman"

#: lib/error.c:96
msgid "error loading module"
msgstr "ralat memuatkan modul"

#: lib/error.c:98
msgid "error resolving symbol in module"
msgstr "ralat menyelesaikan simbol dalam modul"

#: lib/error.c:100
msgid "library/module version mismatch"
msgstr "versi pustaka/modul tidak sepadan"

#: lib/error.c:102
msgid "unlocking would make the password field empty"
msgstr "menyahkunci akan membuatkan medan katalaluan kosong"

#: lib/error.c:105
msgid "invalid attribute value"
msgstr ""

#: lib/error.c:107
msgid "invalid module combination"
msgstr ""

#: lib/error.c:109
msgid "user's home directory not owned by them"
msgstr ""

#: lib/error.c:115
msgid "unknown error"
msgstr "ralat tidak diketahui"

#: lib/misc.c:240
msgid "invalid number"
msgstr ""

#: lib/misc.c:254
msgid "invalid ID"
msgstr ""

#: lib/modules.c:61
#, c-format
msgid "no initialization function %s in `%s'"
msgstr "tiada fungsi permulaan %s dalam `%s'"

#: lib/modules.c:79
#, c-format
msgid "module version mismatch in `%s'"
msgstr "versi modul tidak sepadan dalam `%s'"

#: lib/modules.c:92
#, c-format
msgid "module `%s' does not define `%s'"
msgstr "modul `%s' tidak mentakrif `%s'"

#: lib/prompt.c:88
msgid "error reading terminal attributes"
msgstr "ralat membaca sifat terminal"

#: lib/prompt.c:95 lib/prompt.c:107
msgid "error setting terminal attributes"
msgstr "ralat menetapkan sifat terminal"

#: lib/prompt.c:101
msgid "error reading from terminal"
msgstr "ralat membaca dari terminal"

#: lib/user.c:218
msgid "name is not set"
msgstr "nama tidak ditetapkan"

#: lib/user.c:223
msgid "name is too short"
msgstr "nama adalah terlalu pendek"

#: lib/user.c:228
#, c-format
msgid "name is too long (%zu > %d)"
msgstr "nama adalah terlalu panjang (%zu > %d)"

#: lib/user.c:235
msgid "name contains non-ASCII characters"
msgstr "nama mengandungi aksara bukan-ASCII"

#: lib/user.c:242
msgid "name contains control characters"
msgstr "nama mengandungi aksara kawalan"

#: lib/user.c:249
msgid "name contains whitespace"
msgstr "nama mengandungi ruangputih"

#: lib/user.c:261
msgid "name starts with a hyphen"
msgstr "nama bermula dengan tanda sempang"

#: lib/user.c:272
#, c-format
msgid "name contains invalid char `%c'"
msgstr "nama mengandungi aksara tidak sah `%c'"

#: lib/user.c:308 lib/user.c:360
#, c-format
msgid "user %s has no UID"
msgstr "pengguna %s tidak mempunyai UID"

#: lib/user.c:310
#, c-format
msgid "user %s not found"
msgstr ""

#: lib/user.c:333 lib/user.c:361
#, c-format
msgid "group %s has no GID"
msgstr "kumpulan %s tidak mempunyai GID"

#: lib/user.c:335
#, c-format
msgid "group %s not found"
msgstr ""

#: lib/user.c:355
#, c-format
msgid "user %jd has no name"
msgstr "pengguna %jd tidak mempunyai nama"

#: lib/user.c:356
#, c-format
msgid "group %jd has no name"
msgstr "kumpulan %jd tidak mempunyai nama"

#: lib/user.c:364
msgid "user has neither a name nor an UID"
msgstr "pengguna tidak mempunyai nama mahupun UID"

#: lib/user.c:365
msgid "group has neither a name nor a GID"
msgstr "kumpulan tidak mempunyai nama mahupun GID"

#: lib/user.c:1311
#, c-format
msgid "Refusing to use dangerous home directory `%s' by default"
msgstr ""

#: lib/user.c:2310
#, c-format
msgid "Invalid default value of field %s: %s"
msgstr "Nilai default bagi medan %s tidak sah: %s"

#: lib/util.c:300 modules/files.c:374
#, c-format
msgid "error locking file: %s"
msgstr "ralat mengunci fail: %s"

#: lib/util.c:704
#, c-format
msgid "couldn't get default security context: %s"
msgstr ""

#: lib/util.c:731 lib/util.c:757 lib/util.c:783
#, c-format
msgid "couldn't get security context of `%s': %s"
msgstr "tidak dapat menetapkan konteks keselamatan bagi `%s': %s"

#: lib/util.c:737 lib/util.c:763 lib/util.c:789 lib/util.c:821
#, c-format
msgid "couldn't set default security context to `%s': %s"
msgstr "tidak dapat menetapkan konteks keselamatan default kepada `%s': %s"

#: lib/util.c:813
#, c-format
msgid "couldn't determine security context for `%s': %s"
msgstr ""

#: modules/files.c:129 modules/files.c:692 modules/files.c:1585
#: modules/files.c:1920 modules/files.c:1930 modules/files.c:2012
#: modules/files.c:2023 modules/files.c:2089 modules/files.c:2101
#: modules/files.c:2191 modules/files.c:2200 modules/files.c:2255
#: modules/files.c:2264 modules/files.c:2359 modules/files.c:2368
#, c-format
msgid "couldn't open `%s': %s"
msgstr "tidak dapat membuka `%s': %s"

#: modules/files.c:137 modules/files.c:994 modules/files.c:1187
#: modules/files.c:1329
#, c-format
msgid "couldn't stat `%s': %s"
msgstr "tidak dapat stat `%s': %s"

#: modules/files.c:161
#, c-format
msgid "error creating `%s': %s"
msgstr "ralat mencipta `%s': %s"

#: modules/files.c:169
#, c-format
msgid "Error changing owner of `%s': %s"
msgstr "Ralat menukar pemilik untuk `%s': %s"

#: modules/files.c:175
#, c-format
msgid "Error changing mode of `%s': %s"
msgstr ""

#: modules/files.c:191
#, c-format
msgid "Error reading `%s': %s"
msgstr "Ralat membaca `%s': %s"

#: modules/files.c:206 modules/files.c:217 modules/files.c:305
#: modules/files.c:467
#, c-format
msgid "Error writing `%s': %s"
msgstr "Ralat menulis `%s': %s"

#: modules/files.c:247 modules/files.c:1005 modules/files.c:1195
#: modules/files.c:1338
#, c-format
msgid "couldn't read from `%s': %s"
msgstr "tidak dapat membaca dari `%s': %s"

#: modules/files.c:256
#, c-format
msgid "Invalid contents of lock `%s'"
msgstr ""

#: modules/files.c:261
#, c-format
msgid "The lock %s is held by process %ju"
msgstr ""

#: modules/files.c:269
#, fuzzy, c-format
msgid "Error removing stale lock `%s': %s"
msgstr "Ralat memindahkan %s ke %s: %s.\n"

#: modules/files.c:297
#, fuzzy, c-format
msgid "error opening temporary file for `%s': %s"
msgstr "Ralat membuka `%s': %s.\n"

#: modules/files.c:321
#, c-format
msgid "Cannot obtain lock `%s': %s"
msgstr ""

#: modules/files.c:434
#, fuzzy, c-format
msgid "Error resolving `%s': %s"
msgstr "Ralat membaca `%s': %s"

#: modules/files.c:442
#, fuzzy, c-format
msgid "Error replacing `%s': %s"
msgstr "Ralat membaca `%s': %s"

#: modules/files.c:903
#, c-format
msgid "%s value `%s': `\\n' not allowed"
msgstr ""

#: modules/files.c:910
#, c-format
msgid "%s value `%s': `:' not allowed"
msgstr ""

#: modules/files.c:1014
msgid "entry already present in file"
msgstr "masukan telah ada dalam fail"

#: modules/files.c:1021 modules/files.c:1031 modules/files.c:1041
#: modules/files.c:1393 modules/files.c:1401 modules/files.c:1409
#, c-format
msgid "couldn't write to `%s': %s"
msgstr "tidak dapat menulis ke `%s': %s"

#: modules/files.c:1173
#, c-format
msgid "entity object has no %s attribute"
msgstr "objek entiti tidak mempunyai ciri %s"

#: modules/files.c:1215
msgid "entry with conflicting name already present in file"
msgstr ""

#: modules/files.c:1803
msgid "`:' and `\\n' not allowed in encrypted password"
msgstr ""

#: modules/files.c:1815 modules/ldap.c:1543 modules/ldap.c:1812
msgid "error encrypting password"
msgstr "ralat mengenkrip katalaluan"

#: modules/files.c:2517 modules/ldap.c:2410
#, c-format
msgid "the `%s' and `%s' modules can not be combined"
msgstr ""

#: modules/files.c:2601 modules/files.c:2679
msgid "not executing with superuser privileges"
msgstr "tidak melaksanakan dengan kelebihan superuser"

#: modules/files.c:2692
msgid "no shadow file present -- disabling"
msgstr "tiada fail bayang wujud -- mematikan"

#: modules/ldap.c:199
msgid "error initializing ldap library"
msgstr "ralat memulakan pustaka ldap"

#: modules/ldap.c:210
#, c-format
msgid "could not set LDAP protocol to version %d"
msgstr "tidak dapat menetapkan protokol LDAP ke versi %d"

#: modules/ldap.c:229
msgid "could not negotiate TLS with LDAP server"
msgstr "tidak dapat merunding TLS dengan pelayan LDAP"

#: modules/ldap.c:424
msgid "could not bind to LDAP server"
msgstr "tidak dapat mengikat ke pelayan LDAP"

#: modules/ldap.c:427
#, c-format
msgid "could not bind to LDAP server, first attempt as `%s': %s"
msgstr "tidak dapat mengikat ke pelayan LDAP, cubaan pertama sebagai `%s': %s"

#: modules/ldap.c:1315
#, c-format
msgid "user object had no %s attribute"
msgstr "objek pengguna tidak mempunyai ciri %s"

#: modules/ldap.c:1324
#, c-format
msgid "user object was created with no `%s'"
msgstr "objek pengguna telah dicipta dengan tanpa `%s'"

#: modules/ldap.c:1344
#, c-format
msgid "error creating a LDAP directory entry: %s"
msgstr "ralat mencipta masukan direktori LDAP: %s"

#: modules/ldap.c:1370 modules/ldap.c:1604
#, c-format
msgid "error modifying LDAP directory entry: %s"
msgstr "ralat mengubahsuai masukan direktori LDAP: %s"

#: modules/ldap.c:1395
#, c-format
msgid "error renaming LDAP directory entry: %s"
msgstr "ralat menamakan semula masukan direktori LDAP: %s"

#: modules/ldap.c:1440
#, c-format
msgid "object had no %s attribute"
msgstr "objek tidak mempunyai ciri %s"

#: modules/ldap.c:1456
#, c-format
msgid "error removing LDAP directory entry: %s"
msgstr "ralat membuang masukan direktori LDAP: %s"

#: modules/ldap.c:1506 modules/ldap.c:1521 modules/ldap.c:1635
#: modules/ldap.c:1730
#, c-format
msgid "object has no %s attribute"
msgstr "objek tidak mempunyai ciri %s"

#: modules/ldap.c:1533
msgid "unsupported password encryption scheme"
msgstr ""

#: modules/ldap.c:1658
msgid "no such object in LDAP directory"
msgstr "tiada objek sebegitu dalam direktori LDAP"

#: modules/ldap.c:1670
#, c-format
msgid "no `%s' attribute found"
msgstr "tiada ciri `%s' dijumpai"

#: modules/ldap.c:1843
#, c-format
msgid "error setting password in LDAP directory for %s: %s"
msgstr "ralat menetapkan katalaluan dalam direktori LDAP untuk %s: %s"

#: modules/ldap.c:2446
msgid "LDAP Server Name"
msgstr "Nama Pelayan LDAP"

#: modules/ldap.c:2452
msgid "LDAP Search Base DN"
msgstr "Asas Carian DN LDAP"

#: modules/ldap.c:2458
msgid "LDAP Bind DN"
msgstr "DN Bind LDAP"

#: modules/ldap.c:2465
msgid "LDAP Bind Password"
msgstr "Katalaluan Bind LDAP"

#: modules/ldap.c:2471
msgid "LDAP SASL User"
msgstr "Pengguna LDAP SASL"

#: modules/ldap.c:2478
msgid "LDAP SASL Authorization User"
msgstr "Pengguna Pengesahan LDAP SASL"

#: modules/sasldb.c:132
#, c-format
msgid "Cyrus SASL error creating user: %s"
msgstr "Cyrus SASL ralat mencipta pengguna: %s"

#: modules/sasldb.c:136
#, c-format
msgid "Cyrus SASL error removing user: %s"
msgstr "Cyrus SASL ralat membuang pengguna: %s"

#: modules/sasldb.c:503 modules/sasldb.c:511
#, c-format
msgid "error initializing Cyrus SASL: %s"
msgstr "ralat memulakan Cyrus SASL: %s"

#: python/admin.c:505
msgid "error creating home directory for user"
msgstr "ralat mencipta direktori rumah untuk pengguna"

#: python/admin.c:544 python/admin.c:583
msgid "error removing home directory for user"
msgstr "ralat membuang direktori rumah untuk pengguna"

#: python/admin.c:654
msgid "error moving home directory for user"
msgstr "ralat memindah direktori rumah untuk pengguna"

#: samples/lookup.c:63
#, c-format
msgid "Error initializing %s: %s\n"
msgstr "Ralat memulakan %s: %s\n"

#: samples/lookup.c:76
#, c-format
msgid "Invalid ID %s\n"
msgstr "ID tidak sah %s\n"

#: samples/lookup.c:88
#, c-format
msgid "Searching for group with ID %jd.\n"
msgstr "Mencari untuk kumpulan dengan ID %jd.\n"

#: samples/lookup.c:92
#, c-format
msgid "Searching for group named %s.\n"
msgstr "Mencari untuk kumpulan bernama %s.\n"

#: samples/lookup.c:99
#, c-format
msgid "Searching for user with ID %jd.\n"
msgstr "Mencari untuk pengguna dengan ID %jd.\n"

#: samples/lookup.c:103
#, c-format
msgid "Searching for user named %s.\n"
msgstr "Mencari untuk pengguna bernama %s.\n"

#: samples/lookup.c:117
msgid "Entry not found.\n"
msgstr "Masukan tidak dijumpai.\n"

#: samples/prompt.c:48
msgid "Prompts succeeded.\n"
msgstr "Prompt berjaya.\n"

#: samples/prompt.c:58
msgid "Prompts failed.\n"
msgstr "Prom gagal.\n"

#: samples/testuser.c:76
msgid "Default user object classes:\n"
msgstr "Kelas objek pengguna default:\n"

#: samples/testuser.c:82
msgid "Default user attribute names:\n"
msgstr "Nama atribut pengguna default:\n"

#: samples/testuser.c:88
msgid "Getting default user attributes:\n"
msgstr "Mendapatkan ciri-ciri pengguna default:\n"

#: samples/testuser.c:95
msgid "Copying user structure:\n"
msgstr "Menyalin struktur pengguna:\n"

#~ msgid "backup file `%s' exists and is not a regular file"
#~ msgstr "fail salinan `%s' wujud dan bukan fail biasa"

#~ msgid "backup file size mismatch"
#~ msgstr "saiz fail salinan tidak sepadan"
