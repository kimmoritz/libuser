# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Red Hat, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Andrew Martynov <andrewm@inventa.ru>, 2004, 2005, 2006, 2007
# triplepointfive <iya777@yandex.ru>, 2012
# Leonid Kanter <leon@geon.donetsk.ua>, 2003
# Misha Shnurapet <shnurapet@fedoraproject.org>, 2011
# Miloslav Trmač <mitr@volny.cz>, 2011
# Nikolay Sivov <bunglehead@gmail.com>, 2007
# Stanislav Darchinov <darchinov@gmail.com>, 2011
# Yulia <ypoyarko@redhat.com>, 2008, 2009
# Yulia <yulia.poyarkova@redhat.com>, 2012
# Игорь Горбунов <igor.gorbounov@gmail.com>, 2013
# Леонид Кузин <dg.inc.lcf@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: libuser 0.60\n"
"Report-Msgid-Bugs-To: http://bugzilla.redhat.com/bugzilla/\n"
"POT-Creation-Date: 2015-07-23 21:13+0200\n"
"PO-Revision-Date: 2013-04-29 04:37-0400\n"
"Last-Translator: Miloslav Trmač <mitr@volny.cz>\n"
"Language-Team: Russian (http://www.transifex.com/projects/p/fedora/language/"
"ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Zanata 3.6.2\n"

#: apps/apputil.c:197 apps/apputil.c:201
#, c-format
msgid "Failed to drop privileges.\n"
msgstr "Ошибка сброса привилегий.\n"

#: apps/apputil.c:210
#, c-format
msgid "Internal error.\n"
msgstr "Внутренняя ошибка.\n"

#: apps/apputil.c:231
#, c-format
msgid "%s is not authorized to change the finger info of %s\n"
msgstr "%s не авторизован для изменения finger информации о %s\n"

#: apps/apputil.c:233
msgid "Unknown user context"
msgstr "Неизвестный пользовательский контекст"

#: apps/apputil.c:241
#, c-format
msgid "Can't set default context for /etc/passwd\n"
msgstr "Не могу установить контекст по умолчанию для  /etc/passwd\n"

#: apps/apputil.c:250
#, c-format
msgid "Error initializing PAM.\n"
msgstr "Ошибка инициализации PAM.\n"

#: apps/apputil.c:260 apps/apputil.c:287
#, c-format
msgid "Authentication failed for %s.\n"
msgstr "Ошибка аутентификации для %s.\n"

#: apps/apputil.c:268
#, c-format
msgid "Internal PAM error `%s'.\n"
msgstr "Внутренняя ошибка PAM `%s'.\n"

#: apps/apputil.c:273
#, c-format
msgid "Unknown user authenticated.\n"
msgstr "Аутентификация неизвестного пользователя.\n"

#: apps/apputil.c:277
#, c-format
msgid "User mismatch.\n"
msgstr "Пользователь не совпадает.\n"

#: apps/lchage.c:84 apps/lchfn.c:53 apps/lchsh.c:44 apps/lgroupadd.c:47
#: apps/lgroupdel.c:42 apps/lgroupmod.c:54 apps/lid.c:115 apps/lnewusers.c:45
#: apps/lpasswd.c:47 apps/luseradd.c:55 apps/luserdel.c:45 apps/lusermod.c:56
msgid "prompt for all information"
msgstr "запрашивать по всем данным"

#: apps/lchage.c:86
msgid "list aging parameters for the user"
msgstr "список параметров, которые устаревают, для пользователя"

#: apps/lchage.c:88
msgid "minimum days between password changes"
msgstr "минимум дней между сменой пароля"

#: apps/lchage.c:88 apps/lchage.c:90 apps/lchage.c:93 apps/lchage.c:96
#: apps/lchage.c:99 apps/lchage.c:102
msgid "DAYS"
msgstr "ДНИ"

#: apps/lchage.c:90
msgid "maximum days between password changes"
msgstr "максимум дней между сменой пароля"

#: apps/lchage.c:92
msgid "date of last password change in days since 1/1/70"
msgstr "дата последнего изменения пароля в днях с 1.01.1970"

#: apps/lchage.c:95
msgid ""
"number of days after password expiration date when account is considered "
"inactive"
msgstr ""
"количество дней с даты окончания срока действия пароля, которые должны "
"пройти для того, чтобы учетная запись считалась неактивной"

#: apps/lchage.c:98
msgid "password expiration date in days since 1/1/70"
msgstr "дата окончания срока действия пароля в днях с 1.01.1970"

#: apps/lchage.c:101
msgid "days before expiration to begin warning user"
msgstr ""
"количество дней между завершением срока действия и показом предупреждения "
"пользователю"

#: apps/lchage.c:114 apps/lid.c:130 apps/lpasswd.c:68 apps/luseradd.c:100
#: apps/luserdel.c:59 apps/lusermod.c:101
msgid "[OPTION...] user"
msgstr "[ПАРАМЕТР...] пользователь"

#: apps/lchage.c:117 apps/lchfn.c:70 apps/lchsh.c:58 apps/lgroupadd.c:65
#: apps/lgroupdel.c:56 apps/lgroupmod.c:85 apps/lid.c:133 apps/lnewusers.c:66
#: apps/lpasswd.c:71 apps/luseradd.c:103 apps/luserdel.c:62
#: apps/lusermod.c:104
#, c-format
msgid "Error parsing arguments: %s.\n"
msgstr "Ошибка разбора аргументов: %s.\n"

#: apps/lchage.c:126 apps/lpasswd.c:85 apps/luseradd.c:118 apps/luserdel.c:70
#: apps/lusermod.c:115
#, c-format
msgid "No user name specified.\n"
msgstr "Имя пользователя не указано.\n"

#: apps/lchage.c:138 apps/lchfn.c:103 apps/lchsh.c:91 apps/lgroupadd.c:102
#: apps/lgroupdel.c:77 apps/lgroupmod.c:119 apps/lid.c:179 apps/lnewusers.c:79
#: apps/lpasswd.c:97 apps/luseradd.c:143 apps/luserdel.c:81
#: apps/lusermod.c:157 samples/enum.c:56 samples/testuser.c:71
#, c-format
msgid "Error initializing %s: %s.\n"
msgstr "Ошибка инициализации %s: %s.\n"

#: apps/lchage.c:147 apps/lchfn.c:114 apps/lchsh.c:102 apps/lpasswd.c:148
#: apps/luserdel.c:89 apps/lusermod.c:171
#, c-format
msgid "User %s does not exist.\n"
msgstr "Пользователь %s не существует.\n"

#: apps/lchage.c:160
#, c-format
msgid "Account is locked.\n"
msgstr "Учетная запись заблокирована.\n"

#: apps/lchage.c:162
#, c-format
msgid "Account is not locked.\n"
msgstr "Учетная запись не заблокирована.\n"

#: apps/lchage.c:166
#, c-format
msgid "Minimum:\t%ld\n"
msgstr "Минимум:\t%ld\n"

#: apps/lchage.c:170
#, c-format
msgid "Maximum:\t%ld\n"
msgstr "Максимум:\t%ld\n"

#: apps/lchage.c:172
#, c-format
msgid "Maximum:\tNone\n"
msgstr "Максимум:\tнет\n"

#: apps/lchage.c:175
#, c-format
msgid "Warning:\t%ld\n"
msgstr "Предупреждение:\t%ld\n"

#: apps/lchage.c:180
#, c-format
msgid "Inactive:\t%ld\n"
msgstr "Неактивных:\t%ld\n"

#: apps/lchage.c:182
#, c-format
msgid "Inactive:\tNever\n"
msgstr "Неактивных:\tНикогда\n"

#: apps/lchage.c:186 apps/lchage.c:194 apps/lchage.c:204
msgid "Must change password on next login"
msgstr "Должен сменить пароль при следующем входе"

#: apps/lchage.c:188 apps/lchage.c:196 apps/lchage.c:206 apps/lchage.c:215
msgid "Never"
msgstr "Никогда"

#: apps/lchage.c:191
#, c-format
msgid "Last Change:\t%s\n"
msgstr "Последнее изменение:\t%s\n"

#: apps/lchage.c:201
#, c-format
msgid "Password Expires:\t%s\n"
msgstr "Действие пароля завершается:\t%s\n"

#: apps/lchage.c:213
#, c-format
msgid "Password Inactive:\t%s\n"
msgstr "Пароль не активен:\t%s\n"

#: apps/lchage.c:219
#, c-format
msgid "Account Expires:\t%s\n"
msgstr "Учетная запись действительна до:\t%s\n"

#: apps/lchage.c:240
#, c-format
msgid "Failed to modify aging information for %s: %s\n"
msgstr "Ошибка изменения информации о сроках для %s: %s\n"

#: apps/lchfn.c:67 apps/lchsh.c:55
msgid "[OPTION...] [user]"
msgstr "[ПАРАМЕТР...] [пользователь]"

#: apps/lchfn.c:86 apps/lchsh.c:74 apps/lid.c:164
#, c-format
msgid "No user name specified, no name for uid %d.\n"
msgstr "Имя пользователя не указано, нет имени для uid %d.\n"

#: apps/lchfn.c:96
#, c-format
msgid "Changing finger information for %s.\n"
msgstr "Изменение информации finger для %s.\n"

#: apps/lchfn.c:136
msgid "Full Name"
msgstr "Полное имя"

#: apps/lchfn.c:146
msgid "Surname"
msgstr "Фамилия"

#: apps/lchfn.c:157
msgid "Given Name"
msgstr "Имя"

#: apps/lchfn.c:167
msgid "Office"
msgstr "Офис"

#: apps/lchfn.c:176
msgid "Office Phone"
msgstr "Служебный телефон"

#: apps/lchfn.c:185
msgid "Home Phone"
msgstr "Домашний телефон"

#: apps/lchfn.c:195
msgid "E-Mail Address"
msgstr "Адрес E-Mail"

#: apps/lchfn.c:208
#, c-format
msgid "Finger information not changed:  input error.\n"
msgstr "Информация finger не изменена:  ошибка ввода.\n"

#: apps/lchfn.c:270
msgid "Finger information changed.\n"
msgstr "Информация finger изменена.\n"

#: apps/lchfn.c:273
#, c-format
msgid "Finger information not changed: %s.\n"
msgstr "Информация finger не изменена: %s.\n"

#: apps/lchsh.c:84
#, c-format
msgid "Changing shell for %s.\n"
msgstr "Изменение оболочки для %s\n"

#: apps/lchsh.c:114
msgid "New Shell"
msgstr "Новая оболочка"

#: apps/lchsh.c:121 apps/lchsh.c:136
#, c-format
msgid "Shell not changed: %s\n"
msgstr "Оболочка не изменена: %s\n"

#: apps/lchsh.c:133
msgid "Shell changed.\n"
msgstr "Оболочка изменена.\n"

#: apps/lgroupadd.c:49
msgid "gid for new group"
msgstr "gid для новой группы"

#: apps/lgroupadd.c:49 apps/lgroupmod.c:56 apps/lpasswd.c:56 apps/lpasswd.c:59
#: apps/luseradd.c:67 apps/lusermod.c:66 apps/lusermod.c:68
msgid "NUM"
msgstr "ЧИСЛО"

#: apps/lgroupadd.c:51
msgid "create a system group"
msgstr "создать системную группу"

#: apps/lgroupadd.c:62 apps/lgroupdel.c:53 apps/lgroupmod.c:82
msgid "[OPTION...] group"
msgstr "[ПАРАМЕТР...] группа"

#: apps/lgroupadd.c:74 apps/lgroupdel.c:65 apps/lgroupmod.c:93
#, c-format
msgid "No group name specified.\n"
msgstr "Не указано имя группы.\n"

#: apps/lgroupadd.c:87 apps/lgroupmod.c:105 apps/lnewusers.c:172
#: apps/luseradd.c:164 apps/lusermod.c:127
#, c-format
msgid "Invalid group ID %s\n"
msgstr "Неверен ID группы %s\n"

#: apps/lgroupadd.c:119
#, c-format
msgid "Group creation failed: %s\n"
msgstr "Ошибка создания группы: %s\n"

#: apps/lgroupdel.c:85 apps/lgroupmod.c:132 apps/lpasswd.c:153
#, c-format
msgid "Group %s does not exist.\n"
msgstr "Группа %s не существует.\n"

#: apps/lgroupdel.c:91
#, c-format
msgid "Group %s could not be deleted: %s\n"
msgstr "Группа %s не может быть удалена: %s\n"

#: apps/lgroupmod.c:56
msgid "set GID for group"
msgstr "установить GID для группы"

#: apps/lgroupmod.c:58
msgid "change group to have given name"
msgstr "изменить группу, задав название"

#: apps/lgroupmod.c:58
msgid "NAME"
msgstr "ИМЯ"

#: apps/lgroupmod.c:60 apps/luseradd.c:75
msgid "plaintext password for use with group"
msgstr "не зашифрованный пароль для использования в группе"

#: apps/lgroupmod.c:60 apps/lgroupmod.c:62 apps/lgroupmod.c:64
#: apps/lgroupmod.c:66 apps/lgroupmod.c:68 apps/lgroupmod.c:70
#: apps/lpasswd.c:51 apps/lpasswd.c:53 apps/luseradd.c:59 apps/luseradd.c:61
#: apps/luseradd.c:63 apps/luseradd.c:65 apps/luseradd.c:69 apps/luseradd.c:75
#: apps/luseradd.c:77 apps/luseradd.c:79 apps/luseradd.c:81 apps/luseradd.c:83
#: apps/luseradd.c:85 apps/luseradd.c:87 apps/luseradd.c:89 apps/lusermod.c:58
#: apps/lusermod.c:60 apps/lusermod.c:64 apps/lusermod.c:70 apps/lusermod.c:72
#: apps/lusermod.c:74 apps/lusermod.c:80 apps/lusermod.c:82 apps/lusermod.c:84
#: apps/lusermod.c:86 apps/lusermod.c:88 apps/lusermod.c:90
msgid "STRING"
msgstr "СТРОКА"

#: apps/lgroupmod.c:62 apps/luseradd.c:77
msgid "pre-hashed password for use with group"
msgstr "предварительно зашифрованный пароль для использования в группе"

#: apps/lgroupmod.c:64
msgid "list of administrators to add"
msgstr "список администраторов для добавления"

#: apps/lgroupmod.c:66
msgid "list of administrators to remove"
msgstr "список администраторов для удаления"

#: apps/lgroupmod.c:68
msgid "list of group members to add"
msgstr "список членов группы для добавления"

#: apps/lgroupmod.c:70
msgid "list of group members to remove"
msgstr "список членов группы для удаления"

#: apps/lgroupmod.c:71
msgid "lock group"
msgstr "заблокировать группу"

#: apps/lgroupmod.c:72
msgid "unlock group"
msgstr "разблокировать группу"

#: apps/lgroupmod.c:125 apps/lusermod.c:164
#, c-format
msgid "Both -L and -U specified.\n"
msgstr "-L и -U указаны одновременно.\n"

#: apps/lgroupmod.c:139 apps/lgroupmod.c:148
#, c-format
msgid "Failed to set password for group %s: %s\n"
msgstr "Ошибка установки пароля для группы %s: %s\n"

#: apps/lgroupmod.c:157
#, c-format
msgid "Group %s could not be locked: %s\n"
msgstr "Группа %s не может быть заблокирована: %s\n"

#: apps/lgroupmod.c:166
#, c-format
msgid "Group %s could not be unlocked: %s\n"
msgstr "Группа %s не может быть разблокирована: %s\n"

#: apps/lgroupmod.c:242 apps/lgroupmod.c:257
#, c-format
msgid "Group %s could not be modified: %s\n"
msgstr "Группа %s не может быть изменена: %s\n"

#: apps/lid.c:42 apps/lid.c:74 apps/lid.c:188
#, c-format
msgid "Error looking up %s: %s\n"
msgstr "Ошибка поиска %s: %s\n"

#: apps/lid.c:117
msgid ""
"list members of a named group instead of the group memberships for the named "
"user"
msgstr ""
"список членов по имени группы, а не членство в группе по имени пользователя"

#: apps/lid.c:120
msgid "only list membership information by name, and not UID/GID"
msgstr ""
"список членов только по имени, без личного или группового идентификаторов"

#: apps/lid.c:146
#, c-format
msgid "No group name specified, using %s.\n"
msgstr "Не указано имя группы, используется %s.\n"

#: apps/lid.c:150
#, c-format
msgid "No group name specified, no name for gid %d.\n"
msgstr "Не указано имя группы, нет имени для gid %d.\n"

#: apps/lid.c:160
#, c-format
msgid "No user name specified, using %s.\n"
msgstr "Имя пользователя не указано, используется %s.\n"

#: apps/lid.c:192
#, c-format
msgid "%s does not exist\n"
msgstr "%s не существует.\n"

#: apps/lnewusers.c:47
msgid "file with user information records"
msgstr "файл с записанной информацией о пользователе"

#: apps/lnewusers.c:47
msgid "PATH"
msgstr "ПУТЬ"

#: apps/lnewusers.c:49
msgid "don't create home directories"
msgstr "не создавать домашний каталог"

#: apps/lnewusers.c:51
msgid "don't create mail spools"
msgstr "не отправлять почту в каталог /var/spool"

#: apps/lnewusers.c:63
msgid "[OPTION...]"
msgstr "[ПАРАМЕТР...]"

#: apps/lnewusers.c:88
#, c-format
msgid "Error opening `%s': %s.\n"
msgstr "Ошибка открытия `%s': %s\n"

#: apps/lnewusers.c:118
#, c-format
msgid "Error creating account for `%s': line improperly formatted.\n"
msgstr "Ошибка создания учётной записи для «%s»: неверный формат строки.\n"

#: apps/lnewusers.c:129 apps/luseradd.c:128 apps/lusermod.c:142
#, c-format
msgid "Invalid user ID %s\n"
msgstr "Неверен ID пользователя %s\n"

#: apps/lnewusers.c:136
msgid "Refusing to create account with UID 0.\n"
msgstr "Отказ в создании учетной записи с UID 0.\n"

#: apps/lnewusers.c:206
#, c-format
msgid "Error creating group for `%s' with GID %jd: %s\n"
msgstr "Ошибка создания группы для «%s» с GID %jd: %s\n"

#: apps/lnewusers.c:246
#, c-format
msgid "Refusing to use dangerous home directory `%s' for %s by default\n"
msgstr ""
"Отказ использования небезопасного домашнего каталога «%s» для %s по "
"умолчанию\n"

#: apps/lnewusers.c:257
#, c-format
msgid "Error creating home directory for %s: %s\n"
msgstr "Ошибка создания домашнего каталога для %s: %s\n"

#: apps/lnewusers.c:270
#, c-format
msgid "Error creating mail spool for %s: %s\n"
msgstr "Ошибка создания почтовой очереди для %s: %s\n"

#: apps/lnewusers.c:285
#, c-format
msgid "Error setting initial password for %s: %s\n"
msgstr "Ошибка установки начального пароля для %s: %s\n"

#: apps/lnewusers.c:295
#, c-format
msgid "Error creating user account for %s: %s\n"
msgstr "Ошибка создания учетной записи пользователя %s: %s\n"

#: apps/lpasswd.c:49
msgid "set group password instead of user password"
msgstr "задать пароль для группы вместо пароля для пользователя"

#: apps/lpasswd.c:51
msgid "new plain password"
msgstr "новый простой пароль"

#: apps/lpasswd.c:53
msgid "new crypted password"
msgstr "новый зашифрованный пароль"

#: apps/lpasswd.c:55
msgid "read new plain password from given descriptor"
msgstr "ввести новый пароль "

#: apps/lpasswd.c:58
msgid "read new crypted password from given descriptor"
msgstr "прочесть новый шифрованный пароль, полученный от дешифровщика"

#: apps/lpasswd.c:83
#, c-format
msgid "Changing password for %s.\n"
msgstr "Изменение пароля пользователя %s.\n"

#: apps/lpasswd.c:111
msgid "New password"
msgstr "Новый пароль"

#: apps/lpasswd.c:114
msgid "New password (confirm)"
msgstr "Новый пароль (подтвердите)"

#: apps/lpasswd.c:128
#, c-format
msgid "Passwords do not match, try again.\n"
msgstr "Пароли не совпадают, попробуйте еще раз.\n"

#: apps/lpasswd.c:133
#, c-format
msgid "Password change canceled.\n"
msgstr "Изменение пароля отменено.\n"

#: apps/lpasswd.c:165 apps/lpasswd.c:182
#, c-format
msgid "Error reading from file descriptor %d.\n"
msgstr "Ошибка чтения дескриптора файла %d.\n"

#: apps/lpasswd.c:203 apps/luseradd.c:302 apps/luseradd.c:311
#, c-format
msgid "Error setting password for user %s: %s.\n"
msgstr "Ошибка установки пароля для пользователя %s: %s.\n"

#: apps/lpasswd.c:212
#, c-format
msgid "Error setting password for group %s: %s.\n"
msgstr "Ошибка установки пароля для группы %s: %s.\n"

#: apps/lpasswd.c:224
#, c-format
msgid "Password changed.\n"
msgstr "Пароль изменен.\n"

#: apps/luseradd.c:57
msgid "create a system user"
msgstr "создать системного пользователя"

#: apps/luseradd.c:59
msgid "GECOS information for new user"
msgstr ""
"вспомогательная информация для нового пользователя (номер телефона, адрес, "
"полное имя и т.д.)"

#: apps/luseradd.c:61
msgid "home directory for new user"
msgstr "домашний каталог для нового пользователя"

#: apps/luseradd.c:63
msgid "directory with files for the new user"
msgstr "каталог файлов для нового пользователя"

#: apps/luseradd.c:65
msgid "shell for new user"
msgstr "командная оболочка для нового пользователя"

#: apps/luseradd.c:67
msgid "uid for new user"
msgstr "идентификатор нового пользователя"

#: apps/luseradd.c:69
msgid "group for new user"
msgstr "группа для нового пользователя"

#: apps/luseradd.c:71
msgid "don't create home directory for user"
msgstr "не создавать домашний каталог для пользователя"

#: apps/luseradd.c:73
msgid "don't create group with same name as user"
msgstr "не создавать группу с именем пользователя"

#: apps/luseradd.c:79
msgid "common name for new user"
msgstr "обычное имя пользователя"

#: apps/luseradd.c:81
msgid "given name for new user"
msgstr "имя, задающееся для нового пользователя"

#: apps/luseradd.c:83
msgid "surname for new user"
msgstr "фамилия пользователя"

#: apps/luseradd.c:85
msgid "room number for new user"
msgstr "номер кабинета для нового пользователя"

#: apps/luseradd.c:87
msgid "telephone number for new user"
msgstr "номер телефона пользователя"

#: apps/luseradd.c:89
msgid "home telephone number for new user"
msgstr "домашний номер телефона пользователя"

#: apps/luseradd.c:189
#, c-format
msgid "Group %jd does not exist\n"
msgstr "Группа %jd не существует\n"

#: apps/luseradd.c:207 apps/luseradd.c:220
#, c-format
msgid "Error creating group `%s': %s\n"
msgstr "Ошибка создания группы `%s': %s\n"

#: apps/luseradd.c:260
#, c-format
msgid "Account creation failed: %s.\n"
msgstr "Ошибка создания учетной записи: %s.\n"

#: apps/luseradd.c:283
#, c-format
msgid "Error creating %s: %s.\n"
msgstr "Ошибка создания %s: %s.\n"

#: apps/luseradd.c:290
#, c-format
msgid "Error creating mail spool: %s\n"
msgstr "Ошибка создания почтовой очереди: %s\n"

#: apps/luserdel.c:47
msgid "don't remove the user's private group, if the user has one"
msgstr "не удалять собственные группы пользователей, если они имеются"

#: apps/luserdel.c:50
msgid "remove the user's home directory"
msgstr "удалить домашний каталог пользователя"

#: apps/luserdel.c:94
#, c-format
msgid "User %s could not be deleted: %s.\n"
msgstr "Пользователь %s не может быть удален: %s.\n"

#: apps/luserdel.c:108
#, c-format
msgid "%s did not have a gid number.\n"
msgstr "%s не имеет уровня gid.\n"

#: apps/luserdel.c:114
#, c-format
msgid "No group with GID %jd exists, not removing.\n"
msgstr "Не существует группы с GID %jd, не удаляется.\n"

#: apps/luserdel.c:120
#, c-format
msgid "Group with GID %jd did not have a group name.\n"
msgstr "Группа c GID %jd не имела имени.\n"

#: apps/luserdel.c:126
#, c-format
msgid "Group %s could not be deleted: %s.\n"
msgstr "Группа %s не может быть удалена: %s.\n"

#: apps/luserdel.c:139
#, fuzzy, c-format
msgid "Error removing home directory: %s.\n"
msgstr "ошибка удаления домашнего каталога для пользователя"

#: apps/luserdel.c:145
#, c-format
msgid "Error removing mail spool: %s"
msgstr "Ошибка при удалении почтовой очереди: %s"

#: apps/lusermod.c:58
msgid "GECOS information"
msgstr "дополнительная информация GECOS"

#: apps/lusermod.c:60
msgid "home directory"
msgstr "домашний каталог"

#: apps/lusermod.c:62
msgid "move home directory contents"
msgstr "переместить содержимое домашнего каталога"

#: apps/lusermod.c:64
msgid "set shell for user"
msgstr "назначить командную оболочку для пользователя"

#: apps/lusermod.c:66
msgid "set UID for user"
msgstr "назначить идентификатор пользователя"

#: apps/lusermod.c:68
msgid "set primary GID for user"
msgstr "назначить пользователю основную группу "

#: apps/lusermod.c:70
msgid "change login name for user"
msgstr "изменить имя пользователя"

#: apps/lusermod.c:72
msgid "plaintext password for the user"
msgstr "открытый пароль для пользователя"

#: apps/lusermod.c:74
msgid "pre-hashed password for the user"
msgstr "предварительно зашифрованный пароль для пользователя"

#: apps/lusermod.c:75
msgid "lock account"
msgstr "заблокировать учётную запись"

#: apps/lusermod.c:78
msgid "unlock account"
msgstr "разблокировать учётную запись"

#: apps/lusermod.c:80
msgid "set common name for user"
msgstr "установить общее имя пользователя"

#: apps/lusermod.c:82
msgid "set given name for user"
msgstr "установить полученное имя пользователя"

#: apps/lusermod.c:84
msgid "set surname for user"
msgstr "установить фамилию пользователя"

#: apps/lusermod.c:86
msgid "set room number for user"
msgstr "установить номер кабинета пользователя"

#: apps/lusermod.c:88
msgid "set telephone number for user"
msgstr "установить номер телефона пользователя"

#: apps/lusermod.c:90
msgid "set home telephone number for user"
msgstr "установить номер домашнего телефона пользователя"

#: apps/lusermod.c:180 apps/lusermod.c:193
#, c-format
msgid "Failed to set password for user %s: %s.\n"
msgstr "Ошибка установки пароля для пользователя %s: %s.\n"

#: apps/lusermod.c:203
#, c-format
msgid "User %s could not be locked: %s.\n"
msgstr "Пользователь %s не может быть заблокирован: %s.\n"

#: apps/lusermod.c:211
#, c-format
msgid "User %s could not be unlocked: %s.\n"
msgstr "Пользователь %s не может быть разблокирован: %s.\n"

#: apps/lusermod.c:232
#, c-format
msgid "Warning: Group with ID %jd does not exist.\n"
msgstr "Предупреждение: Группа с номером %jd не существует.\n"

#: apps/lusermod.c:275
#, c-format
msgid "User %s could not be modified: %s.\n"
msgstr "Пользователь %s не может быть изменен: %s.\n"

#: apps/lusermod.c:326
#, c-format
msgid "Group %s could not be modified: %s.\n"
msgstr "Группа %s не может быть изменена: %s.\n"

#: apps/lusermod.c:342
#, c-format
msgid "No old home directory for %s.\n"
msgstr "Нет старого домашнего каталога для %s.\n"

#: apps/lusermod.c:347
#, c-format
msgid "No new home directory for %s.\n"
msgstr "Нет нового домашнего каталога для %s.\n"

#: apps/lusermod.c:353
#, c-format
msgid "Error moving %s to %s: %s.\n"
msgstr "Ошибка перемещения %s в %s: %s.\n"

#: lib/config.c:128
#, c-format
msgid "could not open configuration file `%s': %s"
msgstr "невозможно открыть файл конфигурации `%s': %s"

#: lib/config.c:134
#, c-format
msgid "could not stat configuration file `%s': %s"
msgstr "невозможно получить информацию о файле конфигурации `%s': %s"

#: lib/config.c:143
#, c-format
msgid "configuration file `%s' is too large"
msgstr "файл конфигурации `%s' слишком велик"

#: lib/config.c:159
#, c-format
msgid "could not read configuration file `%s': %s"
msgstr "невозможно прочитать файл конфигурации `%s': %s"

#: lib/error.c:62
msgid "success"
msgstr "выполнено"

#: lib/error.c:64
msgid "module disabled by configuration"
msgstr "модуль запрещен в настройках"

#: lib/error.c:66
msgid "generic error"
msgstr "общая ошибка"

#: lib/error.c:68
msgid "not enough privileges"
msgstr "недостаточно привилегий"

#: lib/error.c:70
msgid "access denied"
msgstr "доступ запрещен"

#: lib/error.c:72
msgid "bad user/group name"
msgstr "неверное имя пользователя/группы"

#: lib/error.c:74
msgid "bad user/group id"
msgstr "неверный ID пользователя/группы"

#: lib/error.c:76
msgid "user/group name in use"
msgstr "имя пользователя/группы используется"

#: lib/error.c:78
msgid "user/group id in use"
msgstr "ID пользователя/группы используется"

#: lib/error.c:80
msgid "error manipulating terminal attributes"
msgstr "ошибка изменения атрибутов терминала"

#: lib/error.c:82
msgid "error opening file"
msgstr "ошибка открытия файла"

#: lib/error.c:84
msgid "error locking file"
msgstr "ошибка блокировки файла"

#: lib/error.c:86
msgid "error statting file"
msgstr "ошибка получения параметров файла"

#: lib/error.c:88
msgid "error reading file"
msgstr "ошибка чтения файла"

#: lib/error.c:90
msgid "error writing to file"
msgstr "ошибка записи в файл"

#: lib/error.c:92
msgid "data not found in file"
msgstr "данные не найдены в файле"

#: lib/error.c:94
msgid "internal initialization error"
msgstr "внутренняя ошибка инициализации"

#: lib/error.c:96
msgid "error loading module"
msgstr "ошибка загрузки модуля"

#: lib/error.c:98
msgid "error resolving symbol in module"
msgstr "ошибка определения символа в модуле"

#: lib/error.c:100
msgid "library/module version mismatch"
msgstr "несоответствие версии модуля и библиотеки"

#: lib/error.c:102
msgid "unlocking would make the password field empty"
msgstr "при разблокировании поле пароля будет очищено"

#: lib/error.c:105
msgid "invalid attribute value"
msgstr "неверное значение аттрибута"

#: lib/error.c:107
msgid "invalid module combination"
msgstr "неверный состав модуля"

#: lib/error.c:109
#, fuzzy
msgid "user's home directory not owned by them"
msgstr "домашний каталог для нового пользователя"

#: lib/error.c:115
msgid "unknown error"
msgstr "неизвестная ошибка"

#: lib/misc.c:240
msgid "invalid number"
msgstr "неверный номер"

#: lib/misc.c:254
msgid "invalid ID"
msgstr "неверный идентификатор"

#: lib/modules.c:61
#, c-format
msgid "no initialization function %s in `%s'"
msgstr "нет функции инициализации %s в `%s'"

#: lib/modules.c:79
#, c-format
msgid "module version mismatch in `%s'"
msgstr "несоответствие версии модуля в `%s'"

#: lib/modules.c:92
#, c-format
msgid "module `%s' does not define `%s'"
msgstr "в модуле `%s' не определено значение `%s'"

#: lib/prompt.c:88
msgid "error reading terminal attributes"
msgstr "ошибка чтения атрибутов терминала"

#: lib/prompt.c:95 lib/prompt.c:107
msgid "error setting terminal attributes"
msgstr "ошибка установки атрибутов терминала"

#: lib/prompt.c:101
msgid "error reading from terminal"
msgstr "ошибка чтения с терминала"

#: lib/user.c:218
msgid "name is not set"
msgstr "имя не установлено"

#: lib/user.c:223
msgid "name is too short"
msgstr "имя слишком коротко"

#: lib/user.c:228
#, c-format
msgid "name is too long (%zu > %d)"
msgstr "имя слишком длинное (%zu > %d)"

#: lib/user.c:235
msgid "name contains non-ASCII characters"
msgstr "имя содержит не-ASCII символы"

#: lib/user.c:242
msgid "name contains control characters"
msgstr "имя содержит управляющие символы"

#: lib/user.c:249
msgid "name contains whitespace"
msgstr "имя содержит пробелы"

#: lib/user.c:261
msgid "name starts with a hyphen"
msgstr "имя начинается с дефиса"

#: lib/user.c:272
#, c-format
msgid "name contains invalid char `%c'"
msgstr "имя содержит неверный символ «%c»"

#: lib/user.c:308 lib/user.c:360
#, c-format
msgid "user %s has no UID"
msgstr "пользователь %s не имеет UID"

#: lib/user.c:310
#, c-format
msgid "user %s not found"
msgstr "пользователь %s не найден"

#: lib/user.c:333 lib/user.c:361
#, c-format
msgid "group %s has no GID"
msgstr "группа %s не имеет GID"

#: lib/user.c:335
#, c-format
msgid "group %s not found"
msgstr "группа %s не найдена"

#: lib/user.c:355
#, c-format
msgid "user %jd has no name"
msgstr "пользователь %jd не имеет имени"

#: lib/user.c:356
#, c-format
msgid "group %jd has no name"
msgstr "группа %jd не имеет имени"

#: lib/user.c:364
msgid "user has neither a name nor an UID"
msgstr "пользователь не имеет имени и кода UID"

#: lib/user.c:365
msgid "group has neither a name nor a GID"
msgstr "группа не имеет имени и кода UID"

#: lib/user.c:1311
#, c-format
msgid "Refusing to use dangerous home directory `%s' by default"
msgstr "Отказ использования небезопасного домашнего каталога «%s» по умолчанию"

#: lib/user.c:2310
#, c-format
msgid "Invalid default value of field %s: %s"
msgstr "Неверное значение по умолчанию для поля %s: %s"

#: lib/util.c:300 modules/files.c:374
#, c-format
msgid "error locking file: %s"
msgstr "ошибка блокировки файла: %s"

#: lib/util.c:704
#, c-format
msgid "couldn't get default security context: %s"
msgstr "невозможно получить контекст безопасности по умолчанию: %s"

#: lib/util.c:731 lib/util.c:757 lib/util.c:783
#, c-format
msgid "couldn't get security context of `%s': %s"
msgstr "невозможно получить контекст безопасности для `%s': %s"

#: lib/util.c:737 lib/util.c:763 lib/util.c:789 lib/util.c:821
#, c-format
msgid "couldn't set default security context to `%s': %s"
msgstr ""
"невозможно задать значение контекста безопасности по умолчанию `%s': %s"

#: lib/util.c:813
#, c-format
msgid "couldn't determine security context for `%s': %s"
msgstr "невозможно определить контекст безопасности для `%s': %s"

#: modules/files.c:129 modules/files.c:692 modules/files.c:1585
#: modules/files.c:1920 modules/files.c:1930 modules/files.c:2012
#: modules/files.c:2023 modules/files.c:2089 modules/files.c:2101
#: modules/files.c:2191 modules/files.c:2200 modules/files.c:2255
#: modules/files.c:2264 modules/files.c:2359 modules/files.c:2368
#, c-format
msgid "couldn't open `%s': %s"
msgstr "невозможно открыть `%s': %s"

#: modules/files.c:137 modules/files.c:994 modules/files.c:1187
#: modules/files.c:1329
#, c-format
msgid "couldn't stat `%s': %s"
msgstr "невозможно получить сведения о `%s': %s"

#: modules/files.c:161
#, c-format
msgid "error creating `%s': %s"
msgstr "ошибка создания `%s': %s"

#: modules/files.c:169
#, c-format
msgid "Error changing owner of `%s': %s"
msgstr "Ошибка при смене владельца «%s»: %s"

#: modules/files.c:175
#, c-format
msgid "Error changing mode of `%s': %s"
msgstr "Ошибка при изменении режима «%s»: %s"

#: modules/files.c:191
#, c-format
msgid "Error reading `%s': %s"
msgstr "Ошибка чтения `%s': %s"

#: modules/files.c:206 modules/files.c:217 modules/files.c:305
#: modules/files.c:467
#, c-format
msgid "Error writing `%s': %s"
msgstr "Ошибка при записи «%s»: %s"

#: modules/files.c:247 modules/files.c:1005 modules/files.c:1195
#: modules/files.c:1338
#, c-format
msgid "couldn't read from `%s': %s"
msgstr "невозможно прочитать из `%s': %s"

#: modules/files.c:256
#, c-format
msgid "Invalid contents of lock `%s'"
msgstr ""

#: modules/files.c:261
#, c-format
msgid "The lock %s is held by process %ju"
msgstr ""

#: modules/files.c:269
#, fuzzy, c-format
msgid "Error removing stale lock `%s': %s"
msgstr "Ошибка перемещения %s в %s: %s.\n"

#: modules/files.c:297
#, fuzzy, c-format
msgid "error opening temporary file for `%s': %s"
msgstr "Ошибка при изменении режима «%s»: %s"

#: modules/files.c:321
#, c-format
msgid "Cannot obtain lock `%s': %s"
msgstr ""

#: modules/files.c:434
#, fuzzy, c-format
msgid "Error resolving `%s': %s"
msgstr "Ошибка чтения `%s': %s"

#: modules/files.c:442
#, fuzzy, c-format
msgid "Error replacing `%s': %s"
msgstr "Ошибка чтения `%s': %s"

#: modules/files.c:903
#, fuzzy, c-format
msgid "%s value `%s': `\\n' not allowed"
msgstr "Значение %s недопустимо `%s' `:'"

#: modules/files.c:910
#, c-format
msgid "%s value `%s': `:' not allowed"
msgstr "Значение %s недопустимо `%s' `:'"

#: modules/files.c:1014
msgid "entry already present in file"
msgstr "запись уже существует в файле"

#: modules/files.c:1021 modules/files.c:1031 modules/files.c:1041
#: modules/files.c:1393 modules/files.c:1401 modules/files.c:1409
#, c-format
msgid "couldn't write to `%s': %s"
msgstr "невозможно записать в `%s': %s"

#: modules/files.c:1173
#, c-format
msgid "entity object has no %s attribute"
msgstr "объект не имеет атрибута %s"

#: modules/files.c:1215
msgid "entry with conflicting name already present in file"
msgstr "запись с конфликтующим именем уже есть в файле"

#: modules/files.c:1803
#, fuzzy
msgid "`:' and `\\n' not allowed in encrypted password"
msgstr "Не допускается использование `:' в паролях"

#: modules/files.c:1815 modules/ldap.c:1543 modules/ldap.c:1812
msgid "error encrypting password"
msgstr "ошибка при шифровании пароля"

#: modules/files.c:2517 modules/ldap.c:2410
#, c-format
msgid "the `%s' and `%s' modules can not be combined"
msgstr "модули `%s' и `%s' не могут быть объединены"

#: modules/files.c:2601 modules/files.c:2679
msgid "not executing with superuser privileges"
msgstr "выполняется без привилегий суперпользователя"

#: modules/files.c:2692
msgid "no shadow file present -- disabling"
msgstr "нет файла shadow -- запрещено"

#: modules/ldap.c:199
msgid "error initializing ldap library"
msgstr "ошибка инициализации библиотеки ldap"

#: modules/ldap.c:210
#, c-format
msgid "could not set LDAP protocol to version %d"
msgstr "невозможно установить протокол LDAP для версии %d"

#: modules/ldap.c:229
msgid "could not negotiate TLS with LDAP server"
msgstr "невозможно согласовать TLS с сервером LDAP"

#: modules/ldap.c:424
msgid "could not bind to LDAP server"
msgstr "невозможно подключиться к серверу LDAP"

#: modules/ldap.c:427
#, c-format
msgid "could not bind to LDAP server, first attempt as `%s': %s"
msgstr "невозможно подключиться к серверу LDAP, первая попытка - `%s': %s"

#: modules/ldap.c:1315
#, c-format
msgid "user object had no %s attribute"
msgstr "объект пользователя не имеет атрибута %s"

#: modules/ldap.c:1324
#, c-format
msgid "user object was created with no `%s'"
msgstr "объект пользователя был создан без `%s'"

#: modules/ldap.c:1344
#, c-format
msgid "error creating a LDAP directory entry: %s"
msgstr "ошибка создания записи каталога LDAP: %s"

#: modules/ldap.c:1370 modules/ldap.c:1604
#, c-format
msgid "error modifying LDAP directory entry: %s"
msgstr "ошибка изменения записи каталога LDAP: %s"

#: modules/ldap.c:1395
#, c-format
msgid "error renaming LDAP directory entry: %s"
msgstr "ошибка переименования записи каталога LDAP: %s"

#: modules/ldap.c:1440
#, c-format
msgid "object had no %s attribute"
msgstr "объект не имел атрибута %s"

#: modules/ldap.c:1456
#, c-format
msgid "error removing LDAP directory entry: %s"
msgstr "ошибка удаления записи каталога LDAP: %s"

#: modules/ldap.c:1506 modules/ldap.c:1521 modules/ldap.c:1635
#: modules/ldap.c:1730
#, c-format
msgid "object has no %s attribute"
msgstr "объект не имеет атрибута %s"

#: modules/ldap.c:1533
msgid "unsupported password encryption scheme"
msgstr "схема шифрования паролей не поддерживается"

#: modules/ldap.c:1658
msgid "no such object in LDAP directory"
msgstr "нет такого объекта в каталоге LDAP"

#: modules/ldap.c:1670
#, c-format
msgid "no `%s' attribute found"
msgstr "не найден атрибут `%s'"

#: modules/ldap.c:1843
#, c-format
msgid "error setting password in LDAP directory for %s: %s"
msgstr "ошибка установки пароля в каталоге LDAP для %s: %s"

#: modules/ldap.c:2446
msgid "LDAP Server Name"
msgstr "Имя сервера LDAP"

#: modules/ldap.c:2452
msgid "LDAP Search Base DN"
msgstr "LDAP Search Base DN"

#: modules/ldap.c:2458
msgid "LDAP Bind DN"
msgstr "LDAP Bind DN"

#: modules/ldap.c:2465
msgid "LDAP Bind Password"
msgstr "LDAP Bind Password"

#: modules/ldap.c:2471
msgid "LDAP SASL User"
msgstr "Пользователь LDAP SASL"

#: modules/ldap.c:2478
msgid "LDAP SASL Authorization User"
msgstr "Авторизованный пользователь LDAP SASL"

#: modules/sasldb.c:132
#, c-format
msgid "Cyrus SASL error creating user: %s"
msgstr "ошибка Cyrus SASL при создании пользователя: %s"

#: modules/sasldb.c:136
#, c-format
msgid "Cyrus SASL error removing user: %s"
msgstr "Ошибка SASL при удалении пользователя: %s"

#: modules/sasldb.c:503 modules/sasldb.c:511
#, c-format
msgid "error initializing Cyrus SASL: %s"
msgstr "ошибка инициализации Cyrus SASL: %s"

#: python/admin.c:505
msgid "error creating home directory for user"
msgstr "ошибка создания домашнего каталога для пользователя"

#: python/admin.c:544 python/admin.c:583
msgid "error removing home directory for user"
msgstr "ошибка удаления домашнего каталога для пользователя"

#: python/admin.c:654
msgid "error moving home directory for user"
msgstr "ошибка перемещения домашнего каталога для пользователя"

#: samples/lookup.c:63
#, c-format
msgid "Error initializing %s: %s\n"
msgstr "Ошибка при инициализации %s: %s.\n"

#: samples/lookup.c:76
#, c-format
msgid "Invalid ID %s\n"
msgstr "Неверен идентификатор %s\n"

#: samples/lookup.c:88
#, c-format
msgid "Searching for group with ID %jd.\n"
msgstr "Поиск группы с номером %jd.\n"

#: samples/lookup.c:92
#, c-format
msgid "Searching for group named %s.\n"
msgstr "Поиск группы с именем %s.\n"

#: samples/lookup.c:99
#, c-format
msgid "Searching for user with ID %jd.\n"
msgstr "Поиск пользователя с номером %jd.\n"

#: samples/lookup.c:103
#, c-format
msgid "Searching for user named %s.\n"
msgstr "Поиск пользователя с именем %s.\n"

#: samples/lookup.c:117
msgid "Entry not found.\n"
msgstr "Запись не найдена\n"

#: samples/prompt.c:48
msgid "Prompts succeeded.\n"
msgstr "Запрос выполнен удачно.\n"

#: samples/prompt.c:58
msgid "Prompts failed.\n"
msgstr "Запрос выполнен неудачно.\n"

#: samples/testuser.c:76
msgid "Default user object classes:\n"
msgstr "Стандартные классы объекта пользователя:\n"

#: samples/testuser.c:82
msgid "Default user attribute names:\n"
msgstr "Имена стандартных атрибутов пользователя:\n"

#: samples/testuser.c:88
msgid "Getting default user attributes:\n"
msgstr "Получение стандартных атрибутов пользователя:\n"

#: samples/testuser.c:95
msgid "Copying user structure:\n"
msgstr "Копирование структуры пользователя:\n"

#~ msgid "backup file `%s' exists and is not a regular file"
#~ msgstr "резервный файл `%s' существует и не является обычным файлом"

#~ msgid "backup file size mismatch"
#~ msgstr "несоответствие размера файла резервной копии"
